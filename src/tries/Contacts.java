package tries;

import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class Contacts
{
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //    https://www.hackerrank.com/challenges/ctci-contacts/problem
    //
    //    We're going to make our own Contacts application! The application must perform two types of operations:
    //
    //    ADD name, where name is a string denoting a contact name. This must store name as a new contact in the
    //    application. FIND partial, where partial is a string denoting a partial name to search the application for.
    //    It must count the number of contactsMap starting with partial and print the count on a new line. Given n
    //    sequential ADD and FIND operations, perform each operation in order.
    //
    //    Input Format
    //
    //    The first line contains a single integer, n, denoting the number of operations to perform.
    //    Each line i of the m subsequent lines contains an operation in one of the two forms defined above.
    //
    //        Constraints
    //
    //    It is guaranteed that name and partial contain lowercase English letters only.
    //    The input doesn't have any duplicate name for the ADD operation.
    //    Output Format
    //
    //    For each FIND partial operation, print the number of contact names starting with partial on a new line.
    //
    //        Sample Input
    //
    //    4
    //    ADD hack
    //    ADD hackerrank
    //    FIND hac
    //    FIND hak
    //
    //    Sample Output
    //
    //    2
    //    0
    //
    //    Explanation
    //
    //    We perform the following sequence of operations:
    //
    //    Add a contact named hack.
    //
    //    Add a contact named hackerrank.
    //
    //    Find and print the number of contact names beginning with hac. There are currently two contact names in the
    //    application and both of them start with hac, so we print 2 on a new line.
    //
    //    Find and print the number of contact names beginning with hak. There are currently two contact names in the
    //    application but neither of them start with hak, so we print 0 on a new line.
    //
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

    enum Operation {
        add {
           @Override
           protected void execute(String str)
           {
               ContactKey key = new ContactKey(str);
               contactsMap.computeIfAbsent(key, s -> new TreeSet<>());
               Set<String> contactsForKey = contactsMap.get(key);
               contactsForKey.add(str);
           }
        },
        find {
            @Override
            protected void execute(String str)
            {
                Set<String> contacts = contactsMap.get(new ContactKey(str));

                if (contacts == null) {
                    System.out.println(0);
                } else {
                    System.out.println(contacts.stream().filter(s -> s.startsWith(str)).count());
                }
            }
        };

        abstract void execute(String str);
    }

    public static class ContactKey implements Comparable
    {
        String prefix;

        ContactKey(String str)
        {
            this.prefix = str.substring(0, 1);
        }

        public String getPrefix()
        {
            return prefix;
        }

        @Override
        public int hashCode()
        {
            return 31 * prefix.hashCode();
        }

        @Override
        public boolean equals(Object that)
        {
            if (this == that)
                return true;

            if (that == null || !(that instanceof ContactKey)) {
                return false;
            }

            ContactKey key = (ContactKey) that;

            return this.getPrefix().equals(key.getPrefix());
        }

        @Override
        public int compareTo(Object o)
        {
            return this.getPrefix().compareTo(((ContactKey)o).getPrefix());
        }
    }

    private final static TreeMap<ContactKey, Set<String>> contactsMap = new TreeMap<>();

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        for(int a0 = 0; a0 < n; a0++){
            String op = in.next();
            String contact = in.next();

            Operation operation = Operation.valueOf(op);
            operation.execute(contact);
        }
    }
}