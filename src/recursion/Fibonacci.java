package recursion;

import java.util.HashMap;

/**
 * https://www.hackerrank.com/challenges/ctci-fibonacci-numbers/problem
 */
public class Fibonacci
{
    private static int STARTING_NUM = 60;

    public static void main(String[] args)
    {
        DynamicFibonacciCalculator calculator = new DynamicFibonacciCalculator();

        // regular order
        for (int i = 0; i <= STARTING_NUM; i++) {
            long startTime = System.currentTimeMillis();
            System.out.println("Dynamic, Round(" + i + "):" + calculator.dynamicFibonacci(i) + ", " + (System.currentTimeMillis() - startTime) + " milliseconds");
        }
        for (int i = 0; i <= STARTING_NUM; i++) {
            long startTime = System.currentTimeMillis();
            System.out.println("Regular, Round(" + i + "):" + fibonacci(i) + ", " + (System.currentTimeMillis() - startTime) + " milliseconds");
        }

        // reverse order
        for (int i = STARTING_NUM; i >= 0; i--) {
            long startTime = System.currentTimeMillis();
            System.out.println("Dynamic, Reverse, Round(" + i + "):" + calculator.dynamicFibonacci(i) + ", " + (System.currentTimeMillis() - startTime) + " milliseconds");
        }
        for (int i = STARTING_NUM; i >= 0; i--) {
            long startTime = System.currentTimeMillis();
            System.out.println("Regular, Reverse, Round(" + i + "):" + fibonacci(i) + ", " + (System.currentTimeMillis() - startTime) + " milliseconds");
        }
    }

    public static long fibonacci(int n)
    {
        if (n <= 0) {
            return 0L;
        }
        if (n == 1) {
            return 1L;
        }

        return fibonacci(n-1) + fibonacci(n-2);
    }

    static class DynamicFibonacciCalculator
    {
        HashMap<Integer, Long> storedAnswers;

        DynamicFibonacciCalculator()
        {
            this.storedAnswers = new HashMap<>();
        }

        private Long dynamicFibonacci(int n)
        {
            if (storedAnswers.containsKey(n)) {
                return storedAnswers.get(n);
            }

            if (n <= 0) {
                storedAnswers.put(0, 0L);
                return 0L;
            }
            if (n == 1) {
                storedAnswers.put(1, 1L);
                return 1L;
            }

            long answer = dynamicFibonacci(n-1) + dynamicFibonacci(n-2);

            storedAnswers.put(n, answer);

            return answer;
        }
    }
}