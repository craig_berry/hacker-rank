package recursion;

import java.util.HashMap;
import java.util.Objects;
import java.util.Scanner;

/**
 * https://www.hackerrank.com/challenges/ctci-recursive-staircase/problem
 */
public class DavisStaircase
{
    private static HashMap<StairCountKey, Integer> storedAnswers = new HashMap<>();

    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        int s = in.nextInt();
        for(int a0 = 0; a0 < s; a0++){
            int n = in.nextInt();
            int waysToClimb = countWaysToClimbStairs(n);
            System.out.println(waysToClimb);
        }
    }

    private static int countWaysToClimbStairs(int stairHeight)
    {
        return countStairsInternal(stairHeight, 1) + countStairsInternal(stairHeight, 2) + countStairsInternal(stairHeight, 3);
    }

    private static int countStairsInternal(int stairHeight, int stepInterval)
    {
        StairCountKey key = new StairCountKey(stairHeight, stepInterval);
        if (storedAnswers.containsKey(key)) {
            return storedAnswers.get(key);
        }

        if (stairHeight <= 0 || stairHeight < stepInterval) {
            storedAnswers.put(key, 0);
            return 0;
        }

        if (stairHeight - stepInterval == 0) {
            storedAnswers.put(key, 1);
            return 1;
        }

        int waysToClimb = countStairsInternal(stairHeight-stepInterval, 1) +
                countStairsInternal(stairHeight-stepInterval, 2) +
                countStairsInternal(stairHeight-stepInterval, 3);

        storedAnswers.put(key, waysToClimb);

        return waysToClimb;
    }

    private static class StairCountKey
    {
        int stairHeight, stepInterval;

        public StairCountKey(int stairHeight, int stepInterval)
        {
            this.stairHeight = stairHeight;
            this.stepInterval = stepInterval;
        }

        @Override
        public boolean equals(Object o)
        {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            StairCountKey that = (StairCountKey) o;
            return stairHeight == that.stairHeight &&
                    stepInterval == that.stepInterval;
        }

        @Override
        public int hashCode()
        {
            return Objects.hash(stairHeight, stepInterval);
        }
    }
}