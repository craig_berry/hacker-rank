package heaps;

import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Scanner;

public class RunningMedian
{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //    FIND THE RUNNING MEDIAN
    //
    //    https://www.hackerrank.com/challenges/ctci-find-the-running-median/problem
    //
    //    The median of a dataset of integers is the midpoint value of the dataset for which an equal number of integers
    //    are less than and greater than the value. To find the median, you must first sort your dataset of integers in
    //    non-decreasing order, then:
    //
    //    If your dataset contains an odd number of elements, the median is the middle element of the sorted sample.
    //    In the sorted dataset {1, 2, 3}, 2 is the median.
    //
    //    If your dataset contains an even number of elements, the median is the average of the two middle elements of
    //    the sorted sample. In the sorted dataset {1, 2, 3 4}, {(2+3)/2=2.5} is the median.
    //    Given an input stream of  integers, you must perform the following task for each  integer:
    //
    //    Add the ith integer to a running list of integers.
    //    Find the median of the updated list (i.e., for the first element through the ith element).
    //    Print the list's updated median on a new line. The printed value must be a double-precision number scaled to
    //    1 decimal place (i.e., 12.3 format).
    //
    //    Input Format
    //
    //    The first line contains a single integer, n, denoting the number of integers in the data stream.
    //    Each line i of the n subsequent lines contains an integer, ai, to be added to your list.
    //
    //    Constraints
    //    1 <= n <= 10^5
    //    0 <= ai <= 10^5
    //
    //    Output Format
    //
    //    After each new integer is added to the list, print the list's updated median on a new line as a single
    //    double-precision number scaled to  decimal place (i.e., `1.3 format).
    //
    //    Sample Input
    //
    //        6
    //        12
    //        4
    //        5
    //        3
    //        8
    //        7
    //    Sample Output
    //
    //        12.0
    //        8.0
    //        5.0
    //        4.5
    //        5.0
    //        6.0
    //
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] a = new int[n];
        for(int a_i=0; a_i < n; a_i++){
            a[a_i] = in.nextInt();
        }

        runningHeap(a);
    }

    private static void runningHeap(int[] arr)
    {
        PriorityQueue<Integer> minHeap = new PriorityQueue<>();
        PriorityQueue<Integer> maxHeap = new PriorityQueue<>(Comparator.reverseOrder());

        for (int i = 0; i < arr.length; i++) {
            addToHeap(arr[i], maxHeap, minHeap);
            System.out.println(getMedian(maxHeap, minHeap));
        }
    }

    private static void addToHeap(int val, PriorityQueue<Integer> maxHeap, PriorityQueue<Integer> minHeap)
    {
        if (maxHeap.size() == 0) {
            maxHeap.add(val);
        } else {
            if (minHeap.size() < maxHeap.size()) {
                if (val > maxHeap.peek()) {
                    minHeap.add(val);
                } else {
                    minHeap.add(maxHeap.poll());
                    maxHeap.add(val);
                }
            } else if (maxHeap.size() < minHeap.size()) {
                if (val < minHeap.peek()) {
                    maxHeap.add(val);
                } else {
                    maxHeap.add(minHeap.poll());
                    minHeap.add(val);
                }
            } else {
                if (val <= maxHeap.peek()) {
                    maxHeap.add(val);
                } else {
                    minHeap.add(val);
                }
            }
        }
    }

    private static double getMedian(PriorityQueue<Integer> minHeap, PriorityQueue<Integer> maxHeap)
    {
        if (minHeap.size() == maxHeap.size()) {
            return (minHeap.peek() + maxHeap.peek()) / 2.0;
        }

        if (minHeap.size() > maxHeap.size()) {
            return minHeap.peek() * 1.0;
        } else {
            return maxHeap.peek() * 1.0;
        }
    }

    /**
     * Need faster implementation
     */
    @Deprecated
    private static double getMedian(List<Integer> runningArr)
    {
        int midpoint = runningArr.size() / 2;

        if (runningArr.size() % 2 != 0) {
            return runningArr.get(midpoint) * 1.0;
        } else {
            return (runningArr.get(midpoint) + runningArr.get(midpoint - 1)) / 2.0;
        }
    }

}