package trees;

public class VerifyBST
{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //    IS THIS A BINARY SEARCH TREE?
    //
    //    https://www.hackerrank.com/challenges/ctci-is-binary-search-tree/problem
    //
    //    For the purposes of this challenge, we define a binary search tree to be a binary tree with the following
    //    ordering properties:
    //
    //    1. The data value of every node in a node's left subtree is less than the data value of that node.
    //    2. The data value of every node in a node's right subtree is greater than the data value of that node.
    //
    //    Given the root node of a binary tree, can you determine if it's also a binary search tree?
    //
    //    Complete the function in your editor below, which has 1 parameter: a pointer to the root of a binary tree. It
    //    must return a boolean denoting whether or not the binary tree is a binary search tree. You may have to write
    //    one or more helper functions to complete this challenge.
    //
    //    Note: We do not consider a binary tree to be a binary search tree if it contains duplicate values.
    //
    //    Input Format
    //
    //    You are not responsible for reading any input from stdin. Hidden code stubs will assemble a binary tree and
    //    pass its root node to your function as an argument.
    //
    //    Constraints
    //    0 <= data <= 10^4
    //
    //    Output Format
    //
    //    You are not responsible for printing any output to stdout. Your function must return true if the tree is a
    //    binary search tree; otherwise, it must return false. Hidden code stubs will print this result as a Yes or No
    //    answer on a new line.
    //
    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void main(String[] args)
    {
        VerifyBST verifier = new VerifyBST();

        Node left = new Node(2, new Node(1), new Node(2));
        Node right = new Node(5);
        Node root = new Node(4, left, right);

        System.out.println(verifier.checkBST(root));
    }

    static class Node
    {
        int data;
        Node left;
        Node right;

        Node(int data)
        {
            this.data = data;
            this.left = null;
            this.right = null;
        }

        Node(int data, Node left, Node right)
        {
            this.data = data;
            this.left = left;
            this.right = right;
        }
    }

    public boolean checkBST(Node root)
    {
        return isValidBSTInternal(root, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    public boolean isValidBSTInternal(Node root, int minLeft, int maxRight)
    {
        if (root == null) {
            return true;
        }

        if (root.data < minLeft || root.data > maxRight) {
            return false;
        }

        return isValidBSTInternal(root.left, minLeft, root.data - 1) && isValidBSTInternal(root.right, root.data + 1, maxRight);
    }
}