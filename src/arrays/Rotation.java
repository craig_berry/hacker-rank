package arrays;

import java.util.*;

public class Rotation {

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //    LEFT ROTATION
    //
    //    https://www.hackerrank.com/challenges/ctci-array-left-rotation/problem
    //
    //    A left rotation operation on an array of size n shifts each of the array's elements 1 unit to the left. For
    //    example, if 2 left rotations are performed on array [1,2,3,4,5], then the array would become [3,4,5,1,2].
    //
    //    Given an array of n integers and a number, d, perform d left rotations on the array. Then print the updated
    //    array as a single line of space-separated integers.
    //
    //    Input Format
    //
    //    The first line contains two space-separated integers denoting the respective values of n (the number of
    //    integers) and d (the number of left rotations you must perform).
    //
    //    The second line contains n space-separated integers describing the respective elements of the array's initial
    //    state.
    //
    //    Constraints
    //    1 <= n <= 10^5
    //    1 <= d <= n
    //    1 <= ai <= 10^6
    //
    //    Output Format
    //
    //    Print a single line of n space-separated integers denoting the final state of the array after performing d
    //    left rotations.
    //
    //    Sample Input
    //
    //     5 4
    //     1 2 3 4 5
    //
    //    Sample Output
    //
    //     5 1 2 3 4
    //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int arrSize = getNextInt(in);
        int numRotations = getNextInt(in);
        int arr[] = new int[arrSize];
        for(int val = 0; val < arrSize; val++){
            arr[val] = getNextInt(in);
        }

        validateInputs(arr, arrSize, numRotations);

        performLeftRotations(arr, numRotations);

        print(arr);
    }

    ////////////////////////////////////////////
    //
    //   Solution
    //
    /////////////////////////////////////////////

    private static void performLeftRotations(int[] arr, int numRotations)
    {
        int[] arrCopy = Arrays.copyOf(arr, arr.length);

        for (int i = 0; i < arr.length; i++) {
            arr[getNewIndex(arrCopy.length, numRotations, i)] = arrCopy[i];
        }
    }

    private static int getNewIndex(int arrLength, int numRotations, int index)
    {
        return ((arrLength - numRotations) + index) % arrLength;
    }

    private static void print(int[] arr)
    {
        Arrays.stream(arr).mapToObj(val -> val + " ").forEach(System.out::print);
    }

    ////////////////////////////////////////////
    //
    //   Helper Methods
    //
    /////////////////////////////////////////////

    private static int getNextInt(Scanner in)
    {
        if (in.hasNext()) {
            return in.nextInt();
        } else {
            throw new RuntimeException("bad input");
        }
    }

    private static void validateInputs(int[] arr, int arrSize, int numRotations)
    {
        validateNumRotations(arr, numRotations);
        validateArrSize(arrSize, arr);

        // todo: validate array values
    }

    private static void validateArrSize(int arrSize, int[] arr)
    {
        if (arrSize < 1 || arrSize > Math.pow(10, 5))
            throw new IllegalArgumentException("Array size does not meet constraints");

        if (arr.length != arrSize)
            throw new IllegalArgumentException("Array input size does not match array length");
    }

    private static void validateNumRotations(int[] arr, int numRotations)
    {
        if (arr.length < numRotations)
            throw new IllegalArgumentException("Number of rotations does not meet constraints");
    }

}
