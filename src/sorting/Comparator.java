package sorting;

import java.util.Arrays;
import java.util.Scanner;

public class Comparator
{
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //    COMPARATOR
    //
    //    https://www.hackerrank.com/challenges/ctci-comparator-sorting/problem
    //
    //    Comparators are used to compare two objects. In this challenge, you'll create a comparator and use it to sort an
    //    array. The Player class is provided in the editor below; it has two fields:
    //
    //    A string, name.
    //    An integer, score.
    //
    //    Given an array of n Player objects, write a comparator that sorts them in order of decreasing score; if 2 or more
    //    players have the same score, sort those players alphabetically by name. To do this, you must create a Checker class
    //    that implements the Comparator interface, then write an int compare(Player a, Player b) method implementing the
    //    Comparator.compare(T o1, T o2) method.
    //
    //    Input Format
    //
    //    Locked stub code in the Solution class handles the following input from stdin:
    //    The first line contains an integer, n, denoting the number of players.
    //    Each of the  subsequent lines contains a player's respective name and score.
    //
    //    Constraints
    //
    //    0 <= score 0<= 1000
    //    Two or more players can have the same name.
    //    Player names consist of lowercase English alphabetic letters.
    //
    //    Output Format
    //
    //    You are not responsible for printing any output to stdout. Locked stub code in Solution will create a Checker
    //    object, use it to sort the Player array, and print each sorted element.
    //
    //    Sample Input
    //
    //    5
    //    amy 100
    //    david 100
    //    heraldo 50
    //    aakansha 75
    //    aleksa 150
    //    Sample Output
    //
    //    aleksa 150
    //    amy 100
    //    david 100
    //    aakansha 75
    //    heraldo 50
    //
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    static class Checker implements java.util.Comparator<Player>
    {
        @Override
        public int compare(Player o1, Player o2)
        {
            if (o1.score == o2.score)
                return o1.name.compareTo(o2.name);

            return o1.score > o2.score ? -1 : 1;
        }
    }

    static class Player
    {
        String name;
        int score;

        Player(String name, int score)
        {
            this.name = name;
            this.score = score;
        }
    }

    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();

        Player[] player = new Player[n];
        Checker checker = new Checker();

        for(int i = 0; i < n; i++){
            player[i] = new Player(scan.next(), scan.nextInt());
        }
        scan.close();

        Arrays.sort(player, checker);
        for (Player aPlayer : player) {
            System.out.printf("%s %s\n", aPlayer.name, aPlayer.score);
        }
    }
}