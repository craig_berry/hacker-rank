package sorting;

import java.util.Scanner;

public class BubbleSort
{
    /////////////////////////////////////////
    //
    //  https://www.hackerrank.com/challenges/ctci-bubble-sort/problem
    //
    //  Requirements
    //
    //  Given an n-element array of distinct elements, sort array in ascending order using
    //  the Bubble Sort algorithm. Once sorted, print the following three lines:
    //
    //  Array is sorted in numSwaps swaps., where  is the number of swaps that took place.
    //  First Element: firstElement, where  is the first element in the sorted array.
    //  Last Element: lastElement, where  is the last element in the sorted array.
    //
    /////////////////////////////////////////

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int a[] = new int[n];
        for(int a_i=0; a_i < n; a_i++){
            a[a_i] = in.nextInt();
        }

        bubbleSort(a);
    }

    private static void bubbleSort(int[] arr)
    {
        boolean sorted = false;
        int swapCnt = 0;

        while(!sorted) {
            sorted = true;

            for (int i = 0; i < arr.length - 1; i++) {
                if (arr[i] > arr[i+1]) {
                    swap(arr, i);
                    sorted = false;
                    swapCnt++;
                }
            }
        }

        System.out.println("Array is sorted in " + swapCnt + " swaps.");
        System.out.println("First Element: " + arr[0]);
        System.out.println("Last Element: " + arr[arr.length-1]);
    }

    private static void swap(int[] arr, int i)
    {
        int temp = arr[i];
        arr[i] = arr[i+1];
        arr[i+1] = temp;
    }
}