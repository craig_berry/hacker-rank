package hashtable;

import java.util.HashMap;
import java.util.Scanner;

/**
 * https://www.hackerrank.com/challenges/ctci-ice-cream-parlor/problem
 */
public class IceCreamParlor
{

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        for(int a0 = 0; a0 < t; a0++){
            int money = in.nextInt();
            int n = in.nextInt();
            int[] arr = new int[n];
            for(int arr_i = 0; arr_i < n; arr_i++){
                arr[arr_i] = in.nextInt();
            }
            solve(arr, money);
        }
        in.close();
    }

    private static void solve(int[] arr, int money)
    {
        HashMap<Integer, Integer> indexMap = new HashMap<>();

        // create map with "flavors" as keys
        for (int i = 0; i < arr.length; i++) {
            indexMap.put(arr[i], i);
        }

        for (int i = 0; i < arr.length; i++) {
            // calc what cost is needed for other flavor
            int val = arr[i];
            int diff = money - val;

            // utilize O(1) lookup time of HashMap
            if (indexMap.containsKey(diff)) {

                // increment indexes for expected output format
                System.out.println((i+1) + " " + (indexMap.get(diff)+1));
                return;
            }
        }
    }
}